﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackendAPI.Models
{
    public class Group
    {
        [JsonProperty("_groupId")]
        public int Id { get; set; }
        [JsonProperty("_name")]

        public string Name { get; set; }
        [JsonProperty("EntityTypes")]

        public List<Entity> Entities { get; set; }
        [JsonProperty("_systemId")]

        public int SystemId { get; set; }
        [JsonProperty("_visible")]

        public bool Visible { get; set; }
    }
}