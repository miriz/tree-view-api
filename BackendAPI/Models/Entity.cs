﻿using BackendAPI.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackendAPI.Models
{
    public class Entity
    {
        public int Id { get; set; }
        [JsonProperty("_name")]

        public string Name { get; set; }
        [JsonProperty("_entityTypeId")]

        public int EntityTypeId { get; set; }
        [JsonProperty("_iconId")]

        public int IconId { get; set; }
        [JsonProperty("Icon")]

        public Icon icon { get; set; }
    }

    public class EntityDTO {
        public int SystemId { get; set; }
        public int GroupId { get; set; }
        public int EntityId { get; set; }
        public string Name { get; set; }
    }

}