﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackendAPI.Models
{
    public class Icon
    {
        [JsonProperty("_iconId")]

        public int Id { get; set; }
        [JsonProperty("_iconName")]

        public string Name { get; set; }
    }
}