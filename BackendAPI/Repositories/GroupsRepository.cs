﻿using BackendAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BackendAPI.Repositories
{
    public class GroupsRepository
    {
        public List<Group> GetGroupsBySystemId(int systemId)
        {
          
            string path = System.Web.HttpContext.Current.Request.MapPath(@"~\App_Data\entityGroupsDb.json");
            var groups = JsonConvert.DeserializeObject<List<Group>>(File.ReadAllText(path));
            return groups.Where(g => g.SystemId == systemId).ToList();
        }
    }
}