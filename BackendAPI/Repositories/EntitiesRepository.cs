﻿using BackendAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BackendAPI.Repositories
{
    public class EntitiesRepository
    {
        public Entity GetEntity(int systemId,int groupId,int id)
        {
            string path = System.Web.HttpContext.Current.Request.MapPath(@"~\App_Data\entityGroupsDb.json");
            var groups = JsonConvert.DeserializeObject<List<Group>>(File.ReadAllText(path));
            return groups.Where(g => g.SystemId == systemId && g.Id == groupId).FirstOrDefault().Entities.Where(e => e.EntityTypeId == id).FirstOrDefault();
        }

        internal void UpdateEntity(EntityDTO data)
        {
            string path = System.Web.HttpContext.Current.Request.MapPath(@"~\App_Data\entityGroupsDb.json");
            var groups = JsonConvert.DeserializeObject<List<Group>>(File.ReadAllText(path));
             groups.FirstOrDefault(g => g.SystemId == data.SystemId && g.Id == data.GroupId).Entities.FirstOrDefault(e => e.EntityTypeId == data.EntityId).Name = data.Name;
            string output = Newtonsoft.Json.JsonConvert.SerializeObject(groups, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText(path, output);
        }
    }
}