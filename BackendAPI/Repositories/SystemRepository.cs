﻿using BackendAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BackendAPI.Repositories
{
    public class SystemRepository
    {

        public List<int> GetSystems()
        {

            string path = System.Web.HttpContext.Current.Request.MapPath(@"~\App_Data\entityGroupsDb.json");
            var groups = JsonConvert.DeserializeObject<List<Group>>(File.ReadAllText(path));
            var systems = groups.Select(g => g.SystemId).Distinct().ToList();
            return systems;
        }
    }
}