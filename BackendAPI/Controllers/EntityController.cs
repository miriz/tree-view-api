﻿using BackendAPI.Models;
using BackendAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BackendAPI.Controllers
{
    public class EntityController : ApiController
    {
        private EntitiesRepository entitiesRepository = new EntitiesRepository();

        // GET: api/Entity
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Entity/5
        public IHttpActionResult Get(int systemId, int groupId, int id)
        {
            try
            {
                var entity = entitiesRepository.GetEntity(systemId, groupId, id);
                return Ok(entity);
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }

       [HttpPut]
        public IHttpActionResult Put([FromBody]EntityDTO data)
        {
            try
            {
               entitiesRepository.UpdateEntity(data);
                return Ok();
            }
            catch (Exception )
            {

                return BadRequest();
            }

        }

        
      
    }
}
