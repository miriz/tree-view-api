﻿using BackendAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BackendAPI.Controllers
{
    [RoutePrefix("api/system")]
    public class SystemController : ApiController
    {
        private GroupsRepository groupsRepository = new GroupsRepository();
        private SystemRepository systemRepository = new SystemRepository();
        // GET: api/System
        public IHttpActionResult Get()
        {
            try
            {
                var systems = systemRepository.GetSystems();
                return Ok(systems);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/System/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var groups = groupsRepository.GetGroupsBySystemId(id);
                return Ok(groups);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // POST: api/System
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/System/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/System/5
        public void Delete(int id)
        {
        }
    }
}
