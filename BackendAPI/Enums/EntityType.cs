﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackendAPI.Enums
{
    public enum eEntityType
    {
        GSensor=2,
        GreenRadar=3,
        AOI=4,
        RedMissile=5
    }
}